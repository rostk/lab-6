import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class KitchenManager {
    private List<KitchenAppliance> appliance = new LinkedList();

    public final void setAppliance(final List<KitchenAppliance> appliance) {
        this.appliance = appliance;

    }


    public final void addAppliance(final KitchenAppliance appliance) {
        this.appliance.add(appliance);
    }


    public final List<KitchenAppliance> sortByPower(List<KitchenAppliance> appliance) {
        appliance.sort(Comparator.comparingDouble(KitchenAppliance::getPower));
        return appliance;
    }


    public final List<KitchenAppliance> findByCompatibility(final DishesType dishesType) {
        List<KitchenAppliance> result = new LinkedList<>();
        for (KitchenAppliance app : appliance) {
            if (dishesType == app.getDishes()) {
                result.add(app);
            }
        }
        return result;
    }


    public final List<KitchenAppliance> getAppliance() {
        return appliance;
    }


}
