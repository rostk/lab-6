public class KitchenAppliance {
    private double power;
    private String compatibility;
    private DishesType dishes;
    private String name;

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public KitchenAppliance(final double power, final String name,
                            final String compatibility, final DishesType dishes) {
        this.power = power;
        this.name = name;
        this.compatibility = compatibility;
        this.dishes = dishes;
    }

    public final String getCompatibility() {
        return compatibility;
    }

    public final DishesType getDishes() {
        return dishes;
    }

    public final double getPower() {
        return power;
    }

    public final void setPower(final double power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "Power = " + power
                + " Compatibility = "
                + compatibility
                + " Dishes = " + dishes
                + " name = " + name;
    }
}