public class Mixer extends KitchenAppliance {
    private int rotate;

    public Mixer(final double power, final String name, final String compatibility,
                 final DishesType dishes, final int rotate) {
        super(power, name, compatibility, dishes);
        this.rotate = rotate;
    }

    public final int getRotate() {
        return rotate;
    }

    public final void setRotate(final int rotate) {
        this.rotate = rotate;
    }

    @Override
    public final String toString() {
        return "Mixer: "
                + "rotate = " + rotate
                + " " + super.toString() + "\n";
    }
}
