import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertNotNull;

public class ToasterTest {
    Toaster toaster = null;


    @Before
    public void setUp() throws Exception {
        toaster = new Toaster(16.23, "Toaster",
                "Bread", DishesType.BREADPRODUCTS, 13);
    }

    @Test
    public void getCookingTime() {
        toaster.setCookingTime(90);
        TestCase.assertEquals(90, toaster.getCookingTime(), 0.00000000001);
    }

    @Test
    public void setCookingTime() {
        toaster.setCookingTime(90);
        TestCase.assertEquals(90, toaster.getCookingTime(), 0.00000000001);
    }

    @Test
    public String toString() {
        assertNotNull(toaster.getCookingTime());
        return null;
    }
}