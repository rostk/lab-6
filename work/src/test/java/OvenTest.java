import org.junit.Before;
import org.junit.Test;
import static junit.framework.TestCase.assertNotNull;
import junit.framework.TestCase;

public class OvenTest {
    Oven oven = null;

    @Before
    public void setUp() throws Exception {
        oven = new Oven(60.42, "Oven",
                "Bakery", DishesType.DESERTS, 90);

    }

    @Test
    public void getTemperature() {
        oven.setTemperature(90);
        TestCase.assertEquals(90, oven.getTemperature(), 0.00000000001);
    }

    @Test
    public void setTemperature() {
        oven.setTemperature(90);
        TestCase.assertEquals(90, oven.getTemperature(), 0.00000000001);
    }

    @Test
    public String toString() {
        assertNotNull(oven.getTemperature());
        return null;
    }
}