import org.junit.Before;
import org.junit.Test;
import junit.framework.TestCase;

import java.util.List;

import static junit.framework.TestCase.assertNotNull;

public class KitchenApplianceTest {
    KitchenAppliance kitchenAppliance = null;
    List<KitchenAppliance> appliance = null;

    @Before
    public void setUp() throws Exception {
        appliance.add(new Juicer(100.32, "Juicer",
                "Juice", DishesType.JUICE, 12));
        appliance.add(new Mixer(50.45, "Mixer",
                "Cakes", DishesType.CAKES, 13));
        appliance.add(new Oven(60.42, "Oven",
                "Bakery", DishesType.DESERTS, 90));
        appliance.add(new Toaster(16.23, "Toaster",
                "Bread", DishesType.BREADPRODUCTS, 13));
    }

    @Test
    public void getName() {
        kitchenAppliance.setName("Juicer");
        TestCase.assertEquals("Juicer", kitchenAppliance.getName(), 0.00000000001);
        kitchenAppliance.setName("Mixer");
        TestCase.assertEquals("Mixer", kitchenAppliance.getName(), 0.00000000001);
        kitchenAppliance.setName("Oven");
        TestCase.assertEquals("Oven", kitchenAppliance.getName(), 0.00000000001);
        kitchenAppliance.setName("Toaster");
        TestCase.assertEquals("Toaster", kitchenAppliance.getName(), 0.00000000001);
    }

    @Test
    public void setName() {
        kitchenAppliance.setName("Juicer");
        TestCase.assertEquals("Juicer", kitchenAppliance.getName(), 0.00000000001);
        kitchenAppliance.setName("Mixer");
        TestCase.assertEquals("Mixer", kitchenAppliance.getName(), 0.00000000001);
        kitchenAppliance.setName("Oven");
        TestCase.assertEquals("Oven", kitchenAppliance.getName(), 0.00000000001);
        kitchenAppliance.setName("Toaster");
        TestCase.assertEquals("Toaster", kitchenAppliance.getName(), 0.00000000001);
    }


    @Test
    public void getPower() {
        kitchenAppliance.setPower(100.32);
        TestCase.assertEquals(100.32, kitchenAppliance.getPower(),0.00000000001);
        kitchenAppliance.setPower(50.45);
        TestCase.assertEquals( 50.45, kitchenAppliance.getPower(),0.00000000001);
        kitchenAppliance.setPower( 60.42);
        TestCase.assertEquals(60.42, kitchenAppliance.getPower(),0.00000000001);
        kitchenAppliance.setPower(16.23);
        TestCase.assertEquals(16.23, kitchenAppliance.getPower(),0.00000000001);
    }

    @Test
    public void setPower() {
        kitchenAppliance.setPower(100.32);
        TestCase.assertEquals(100.32, kitchenAppliance.getPower(),0.00000000001);
        kitchenAppliance.setPower(50.45);
        TestCase.assertEquals( 50.45, kitchenAppliance.getPower(),0.00000000001);
        kitchenAppliance.setPower( 60.42);
        TestCase.assertEquals(60.42, kitchenAppliance.getPower(),0.00000000001);
        kitchenAppliance.setPower(16.23);
        TestCase.assertEquals(16.23, kitchenAppliance.getPower(),0.00000000001);
    }

    @Test
    public String toString() {
        assertNotNull(kitchenAppliance.getPower());
        assertNotNull(kitchenAppliance.getCompatibility());
        assertNotNull(kitchenAppliance.getDishes());
        assertNotNull(kitchenAppliance.getName());
        return null;
    }
}