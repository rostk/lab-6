import junit.framework.TestCase;

import static junit.framework.TestCase.assertNotNull;

public class JuicerTest {
    Juicer juicer = null;

    @org.junit.Before
    public void setUp() throws Exception {
        juicer = new Juicer(100.32, "Juicer",
                "Juice", DishesType.JUICE, 12);
    }


    @org.junit.Test
    public void getCapacity() {
        juicer.setCapacity(12);
        TestCase.assertEquals(12, juicer.getCapacity(), 0.00000000001);

    }

    @org.junit.Test
    public void setCapacity() {
        juicer.setCapacity(12);
        TestCase.assertEquals(12, juicer.getCapacity(), 0.00000000001);
    }

    @org.junit.Test
    public String toString() {
        assertNotNull(juicer.getCapacity());
        return null;
    }
}