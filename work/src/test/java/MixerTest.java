import org.junit.Before;
import org.junit.Test;
import static junit.framework.TestCase.assertNotNull;
import junit.framework.TestCase;

public class MixerTest {
    Mixer mixer = null;
    @Before
    public void setUp() throws Exception {
        mixer = new Mixer(50.45, "Mixer",
                "Cakes", DishesType.CAKES, 13);
    }

    @Test
    public void getRotate() {
        mixer.setRotate(13);
        TestCase.assertEquals(13, mixer.getRotate(), 0.00000000001);
    }

    @Test
    public void setRotate() {
        mixer.setRotate(13);
        TestCase.assertEquals(13, mixer.getRotate(), 0.00000000001);
    }

    @Test
    public String toString() {
        assertNotNull(mixer.getRotate());
        return null;

    }
}